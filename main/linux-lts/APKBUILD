# Maintainer: Natanael Copa <ncopa@alpinelinux.org>

_flavor=lts
pkgname=linux-${_flavor}
pkgver=5.15.2
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=1
pkgdesc="Linux lts kernel"
url="https://www.kernel.org"
depends="mkinitfs"
_depends_dev="perl gmp-dev mpc1-dev mpfr-dev elfutils-dev bash flex bison zstd"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any openssl1.1-compat-dev
	diffutils findutils zstd"
options="!strip"
_config=${config:-config-lts.${CARCH}}
install=
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz
	0001-powerpc-config-defang-gcc-check-for-stack-protector-.patch
	vmlinux-zstd.patch

	config-lts.aarch64
	config-lts.armv7
	config-lts.x86
	config-lts.x86_64
	config-lts.ppc64le
	config-lts.s390x

	config-virt.aarch64
	config-virt.armv7
	config-virt.ppc64le
	config-virt.x86
	config-virt.x86_64
	"
subpackages="$pkgname-dev:_dev:$CBUILD_ARCH"
_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors ${_f}"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-${_f}::$CBUILD_ARCH linux-${_f}-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

if [ "${pkgver%.0}" = "$pkgver" ]; then
	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz"
fi
arch="all !armhf !riscv64"
license="GPL-2.0"

_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
mips*) _carch="mips" ;;
ppc*) _carch="powerpc" ;;
s390*) _carch="s390" ;;
esac

# secfixes:
#   5.10.4-r0:
#     - CVE-2020-29568
#     - CVE-2020-29569

prepare() {
	local _patch_failed=
	cd "$srcdir"/linux-$_kernver
	if [ "$_kernver" != "$pkgver" ]; then
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N
	fi

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.${CARCH}
		local _builddir="$srcdir"/build-$i.$CARCH
		mkdir -p "$_builddir"
		echo "-$pkgrel-$i" > "$_builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$_builddir"/.config
		make -C "$srcdir"/linux-$_kernver \
			O="$_builddir" \
			ARCH="$_carch" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$srcdir"/build-$i.$CARCH
		make ARCH="$_carch" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=${pkgver}-${pkgrel}-${_buildflavor}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/${_abi_release}/build \
		"$_outdir"/lib/modules/${_abi_release}/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package lts "$pkgdir"
}

# subflavors install in $subpkgdir
virt() {
	_package virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=${pkgver}-${pkgrel}-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-${_abi_release}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.${CARCH} "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine

	make -j1 -C "$srcdir"/linux-$_kernver O="$dir" ARCH="$_carch" \
		syncconfig prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the soruces
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$srcdir"/linux-$_kernver
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find $(find arch -name include -type d -print) -type f \
		| cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	mkdir -p "$subpkgdir"/lib/modules/${_abi_release}
	ln -sf /usr/src/linux-headers-${_abi_release} \
		"$subpkgdir"/lib/modules/${_abi_release}/build
}

sha512sums="
d25ad40b5bcd6a4c6042fd0fd84e196e7a58024734c3e9a484fd0d5d54a0c1d87db8a3c784eff55e43b6f021709dc685eb0efa18d2aec327e4f88a79f405705a  linux-5.15.tar.xz
214c54a839ae37849715520f4b1049f0df5366ca32522701b43afecfad116794c4542940ba32d389f28f2549d08c03d148f884cb8e565b75aa3c0cad6a4887b7  0001-powerpc-config-defang-gcc-check-for-stack-protector-.patch
d26d3f99fdcbd0f56e9af32a281870bbfd9fe6a12d17921ef3876e72bd1e92a3c131e06567078a45c11a41826b39d3068cc6f0e89f67d9e16a14825984869268  vmlinux-zstd.patch
391fda2a18a8feae8004e9733d4314ab33af30651d7c17adafc37085c52e3ef551c39884f52ab01c0a42fdd4f4350f0aefe478dd76c4f404dcec074b6188353c  config-lts.aarch64
a81f14551169bcb4ce4e3b25c7d5330df95ebb2b0e071e212a7af1b005b3139d352eb49b0f9330a8d321c8a5d9ea4b136013310c8e6e500ff98199e0a68cf054  config-lts.armv7
a510f8fbe111ac6b937caff091c2eb8fc818de172b5f0ea0b9f28c0691513b0a778f2ee99c3c6414f0e716577c291d9b5e93cd7e30316c369b192dccebb6d513  config-lts.x86
229f9c8f31220848e16c49e0ab8603fe69bf6e9347b04ef12b9b3290d222e4683ea74816cd3bf544156e63d69a6430ca6b3a14105873d0af5899a68e807c9145  config-lts.x86_64
a7156dabd323294eaae87abd91ecb92d34930345a956eb9339852b159a98a3e679d85a586410c82a97cf6a1f035e1641698f2712b14e34495aeecc2c6587089a  config-lts.ppc64le
19d4c7c9ce774f0bb09a3723d1fad7af46402e48d39f4d89b4c348fbb332bbeed7890c29e2043f2b7f12fa07dbe5407dd2fb64e9e2bb8bcb36034e69b0f41b08  config-lts.s390x
6a876bb5b79532850425bb5866291753e3a3ad922fe58f756dbf2829de22607010fe1a65584499fd5a1e6cde096ead8880feadff62470dcbcf7e1d08c6c897b4  config-virt.aarch64
f2521927a8954907ae71716791e6c7795bc6c7dc193c9fd248ddb4b8da631a248fe091316da09c757aacd26375ee6090527e802d2502807c2c9e5a476d87e2c3  config-virt.armv7
dd7fcb319a15db92ad003424d3616d9c38c73d97e3ac695f32dd44d76f1f12a3ff4c462c733c2771d10b1ba5bc846fa517ddbe329a88ef2472c8b0ed9d5f348f  config-virt.ppc64le
fabe0211491022dd663df0ebbc4ccd5beddbf28b63a26d8584dce57d8278150fb3aaea5b28cea3a069948af90d20ae8c151ae780d74821e025c6f79742a48974  config-virt.x86
a66b3babd5b978f5e61a3794bb47b9b929538580c96d0dddf3e7f024c0a5ebef132973df715f5817ca735a17f11402fb3d2d135ec766d3d1c47d1dcde9e626c5  config-virt.x86_64
5f0123bdc7c9875e7b3f02a89496a8a1e0808d77dc58fb725e250d93d69510a1ef6462cfb38cb38e78e20ca34fd7446f58327cad5e67fc68ec36d15777048edf  patch-5.15.2.xz
"
