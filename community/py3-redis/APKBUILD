# Maintainer: Eivind Uggedal <eu@eju.no>
pkgname=py3-redis
_pkgname=redis
pkgver=3.5.3
pkgrel=0
pkgdesc="Python3 client for Redis key-value store"
url="https://github.com/andymccurdy/redis-py"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-mock redis"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # Requires running Redis server, unreliable

replaces="py-redis" # Backwards compatibility
provides="py-redis=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	redis-server --dir "$builddir"&
	local _redispid=$!

	python3 setup.py test || (kill $_redispid && false)
	kill $_redispid
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
eae6ac6b3e0f1366a9c29e5e2d9a5210e4627e62e3006159b1c290f36cd9c1c2c12cafe0944f7a010e04632b517874f230aa9411491993b7ecf2080546217206  redis-3.5.3.tar.gz
"
