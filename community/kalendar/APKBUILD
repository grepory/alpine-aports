# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalendar
pkgver=0.2.0
pkgrel=0
pkgdesc="A calendar application using Akonadi to sync with external services (NextCloud, GMail, ...)"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> akonadi
# ppc64le blocked by kaccounts-integration -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://invent.kde.org/pim/kalendar"
license="GPL-3.0-or-later AND BSD-2-Clause"
depends="
	kdepim-runtime
	kirigami2
	qt5-qtlocation
	"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	eventviews-dev
	extra-cmake-modules
	kcalendarcore-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami2-dev
	kitemmodels-dev
	kpackage-dev
	kpeople-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/kalendar/kalendar-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f355d0d11c825724c1e956f909e130a64e614ac89db40d7b502bcc13b8ba53680fc48aab539ebaf51fc8cf4a6cd6b2b966fc90ecc655cb5466cdc81871f838b6  kalendar-0.2.0.tar.xz
"
